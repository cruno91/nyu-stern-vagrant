#! /bin/bash
echo "Provisioning virtual machine..."

TIMEZONE="America/New_York"

echo "Setting timezone..."
echo $TIMEZONE | sudo tee /etc/timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata

# Update Ubuntu 12.04 32-bit
echo "Updating apt-get repositories"
apt-get update -y
echo "Installing python-software-properties and build-essential"
apt-get install python-software-properties build-essential -y
echo "Adding ondrej apache/php repositories"
add-apt-repository ppa:ondrej/apache
add-apt-repository ppa:ondrej/php5 -y
echo "Updating apt-get repositories again"
apt-get update -y

# Install Apache
echo "Installing Apache"
apt-get install apache2 -y

# Install MySQL & required installation dependencies
echo "Installing debconf-utils"
sudo apt-get install debconf-utils -y
echo "Setting mysql root password"
debconf-set-selections <<< "mysql-server mysql-server/root_password password root"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root"
echo "Installing MySQL"
sudo apt-get install mysql-server -y

# Setup MySQL database and users
# Privileges need to be re-granted and flushed after provisioning
echo -e "Creating mysql user stern, databases nyustern & portal, and importing databases"
echo "Creating MySQL user stern"
echo -e "CREATE USER 'stern'@'localhost' IDENTIFIED BY '/L8$m8/';" | mysql -uroot -proot
echo "Creating MySQL database nyustern"
echo -e "CREATE DATABASE nyustern;" | mysql -uroot -proot
echo "Granting privileges to new user stern and root"
echo -e "GRANT ALL ON nyustern.* TO 'stern'@'localhost' IDENTIFIED BY '/L8$m8/'; FLUSH PRIVILEGES;" | mysql -uroot -proot
echo -e "GRANT ALL PRIVILEGES ON nyustern.* TO 'stern'@'localhost' IDENTIFIED BY '/L8$m8/'; FLUSH PRIVILEGES;" | mysql -uroot -proot
echo -e "GRANT ALL ON nyustern.* TO 'root'@'localhost' IDENTIFIED BY 'root'; FLUSH PRIVILEGES;" | mysql -uroot -proot
echo -e "GRANT ALL PRIVILEGES ON nyustern.* TO 'root'@'localhost' IDENTIFIED BY 'root'; FLUSH PRIVILEGES;" | mysql -uroot -proot
echo "Importing nyustern database dump"
echo -e "USE nyustern; SOURCE /var/www/html/setup/databases/dump.sql;" | mysql -uroot -proot

echo "Creating MySQL database portal"
echo -e "CREATE DATABASE portal;" | mysql -uroot -proot
echo "Granting privileges to root"
echo -e "GRANT ALL ON portal.* TO 'root'@'localhost' IDENTIFIED BY 'root'; FLUSH PRIVILEGES;" | mysql -uroot -proot
echo -e "GRANT ALL PRIVILEGES ON portal.* TO 'root'@'localhost' IDENTIFIED BY 'root'; FLUSH PRIVILEGES;" | mysql -uroot -proot
echo "Importing portal database dump"
echo -e "USE portal; SOURCE /var/www/html/setup/databases/portal.sql;" | mysql -uroot -proot

# Change MySQL bind address to allow connections from anywhere
echo "Allowing MySQL connections from anywhere"
sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

# Restart the SQL service
echo "Restarting MySQL"
sudo service mysql restart

echo "Installing PHP & various PHP modules"
# Install PHP
apt-get install php5-common php5-dev libapache2-mod-php5 php5-cli php5-fpm -y
apt-get install curl php5-curl php5-gd php5-mcrypt php5-mysql -y

# Enable apache mods
echo "Enabling mod-rewrite"
a2enmod rewrite
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini

# Create vhost file
echo "Copying vhost to sites-available"
cp /var/www/html/setup/nyu_vhost /etc/apache2/sites-available/www.stern.nyu.npvm.conf
echo "Enabling nyustern"
sudo a2ensite www.stern.nyu.npvm
echo "Restarting apache"
sudo service apache2 restart
echo "Disabling default site"
sudo a2dissite 000-default
echo "Removing default index.html page"
sudo rm /var/www/html/index.html
echo "Restarting apache"
sudo service apache2 restart

# Install Composer
echo "Installing Composer"
curl -sS https://getcomposer.org/installer | php
echo "Moving composer to usr/local/bin"
sudo mv composer.phar /usr/local/bin/composer
echo "Adding composer to PATH"
sed -i '1i export PATH="/home/vagrant/.composer/vendor/bin:$PATH"' /home/vagrant/.bashrc

# Install Drush
# This needs to be re-done after provisioning
echo "Installing Drush"
/usr/local/bin/composer global require drush/drush:6.*
echo "Creating an alias of drush to /usr/local/bin"
ln -s /home/vagrant/.composer/vendor/drush/drush/drush /usr/local/bin/drush
echo "Adding drush to PATH"
echo 'export PATH="/home/vagrant/.composer/vendor/drush/drush/drush:$PATH"' >> /home/vagrant/.bashrc

echo "Copying .acquia & .drush folders to home folder"
cp -r /var/www/html/setup/acquiacloud/.acquia /home/vagrant/.acquia
cp -r /var/www/html/setup/acquiacloud/.drush /home/vagrant/.drush
sudo chmod -R 777 /home/vagrant/.drush/
sudo chmod -R 777 /home/vagrant/.acquia/

echo "Adding alias for drushp command"
echo 'alias drushp="drush --uri=portal-stern.nyu.npvm"' >> /home/vagrant/.bashrc

echo "Sourcing bashrc"
source /home/vagrant/.bashrc

echo "=================Provisioning Complete!=================================="
